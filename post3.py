from flask import Flask,send_file,redirect
from flask import request
from flask import render_template
import MySQLdb

import os
import json
from os import path
from bs4 import BeautifulSoup
import re
import string

app = Flask(__name__)

@app.route('/')
def my_form():
    return send_file("./index.html")

@app.route('/add', methods=['POST'])
def my_form_post():
	text = request.form['text']
	dirr=request.form['dir']
	subject_id=request.form['sub_id']
	# subject_id=request.form['sub_id2']
	# tag_id=request.form['tag_id']
	class_id=request.form['class_id']
	
	# unitId=request.form['unit_id']
	# chapterId=request.form['chapter_id']
	# topicId=request.form['topic_id']

	db = MySQLdb.connect("localhost","root","12345",text)
	cursor=db.cursor()
	sql="SELECT max(id) FROM units"
	sql1="SELECT max(id) FROM `chapters`"
	sql2="SELECT max(id) FROM `topics`"
	try:
		cursor.execute(sql)
		unitId = cursor.fetchone();
		cursor.execute(sql1)
		chapterId=cursor.fetchone();
		cursor.execute(sql2)
		topicId=cursor.fetchone();
		db.commit()
	except:
		db.rollback()	


	def tag_insert(question_id,tagged_id,tag_type):

		tags_cursor = db.cursor()

		sql = "INSERT INTO tagsofquestion(question_id,tagged_id,tag_type) VALUES (%s,%s,%s)"
		try:
			tags_cursor.execute(sql,([question_id],[tagged_id],[tag_type]))
			db.commit()
		except:
			db.rollback()

	def get_LOD_id(name):

		main_id = 0
		check_LOD_cursor = db.cursor()
		sql = """SELECT id,difficulty FROM difficulty WHERE difficulty = '%s'""" % (name)
		check_LOD_cursor.execute(sql)
		check_LOD_results = check_LOD_cursor.fetchall()

		for x in check_LOD_results:
			main_id = x[0]

		return main_id

	def get_unit_id(name):

		main_id = 0
		check_unit_cursor = db.cursor()
		sql = """SELECT id,name FROM units WHERE id > '%s' AND name = '%s'""" % (unitId,name)
		check_unit_cursor.execute(sql)
		check_unit_results = check_unit_cursor.fetchall()

		for x in check_unit_results:
			main_id = x[0]

		return main_id


	def get_chapter_id(name):

		main_id = 0
		check_chapter_cursor = db.cursor()
		sql = """SELECT id,name FROM chapters WHERE id > '%s' AND name = '%s'""" % (chapterId,name)
		check_chapter_cursor.execute(sql)
		check_chapter_results = check_chapter_cursor.fetchall()

		for x in check_chapter_results:
			main_id = x[0]

		return main_id	


	def get_topic_id(name):

		main_id = 0
		check_topic_cursor = db.cursor()
		sql = """SELECT id,name FROM topics WHERE id > '%s' AND name = '%s'""" % (topicId,name)
		check_topic_cursor.execute(sql)
		check_topic_results = check_topic_cursor.fetchall()

		for x in check_topic_results:
			main_id = x[0]

		return main_id

	question_papers=['SSC MOCK PAPER -193.json','SSC MOCK PAPER -193 (A).json','SSC MOCK PAPER -193 (B).json']
	tag_name=['MO193','MO193(A)','MO193(B)']
	get_questions_dir = dirr
	files = [f for f in os.listdir(get_questions_dir)]
	print files

	for main in range(len(files)):
		print files[main]
		for o in range(len(question_papers)):
			if question_papers[o]==files[main]:
				tagging_name=tag_name[o]
				print tagging_name
		tagging_cursor=db.cursor()
		tagging_sql="SELECT id from tags where tag='%s'"%(tagging_name)
		try:
			tagging_cursor.execute(tagging_sql)
			tagging_id=tagging_cursor.fetchall()
			db.commit()
		except:
			db.rollback()

		# tagging_cursor=db.cursor()
		# tagging_sql="SELECT tag_id from tagging where file_name='%s'"%(files[main])
		# try:
		# 	tagging_cursor.execute(tagging_sql)
		# 	taging_result=tagging_cursor.fetchall()
		# 	db.commit()
		# except:
		# 	db.rollback()

		#Check from the DB whether difficulty is present or not

		check_LOD_cursor = db.cursor()
		sql = "SELECT id,difficulty FROM difficulty"
		check_LOD_cursor.execute(sql)
		check_LOD_results = check_LOD_cursor.fetchall()
		check_LOD_results_array = []

		for x in check_LOD_results:
			if x[1] not in check_LOD_results_array:
				check_LOD_results_array.append(x[1])

		#Check from the DB whether difficulty is present or not

		#Check from the DB whether units is present or not

		check_unit_cursor = db.cursor()
		sql = "SELECT id,name FROM units WHERE id > '%s'"%(unitId)
		check_unit_cursor.execute(sql)
		check_unit_results = check_unit_cursor.fetchall()
		check_unit_results_array = []

		for x in check_unit_results:
			if x[1] not in check_unit_results_array:
				check_unit_results_array.append(x[1])

		#Check from the DB whether units is present or not


		#Check from the DB whether chapters is present or not

		check_chapter_cursor = db.cursor()
		sql = "SELECT id,name FROM chapters WHERE id > '%s'"%(chapterId)
		check_chapter_cursor.execute(sql)
		check_chapter_results = check_chapter_cursor.fetchall()
		check_chapter_results_array = []

		for x in check_chapter_results:
			if x[1] not in check_chapter_results_array:
				check_chapter_results_array.append(x[1])

		#Check from the DB whether chapters is present or not


		#Check from the DB whether topics is present or not

		check_topic_cursor = db.cursor()
		sql = "SELECT id,name FROM topics WHERE id > '%s'"%(topicId)
		check_topic_cursor.execute(sql)
		check_topic_results = check_topic_cursor.fetchall()
		check_topic_results_array = []

		for x in check_topic_results:
			if x[1] not in check_topic_results_array:
				check_topic_results_array.append(x[1])

		#Check from the DB whether topics is present or not


		with open(get_questions_dir+files[main]) as data_file:
			data = json.loads(data_file.read())

		LOD_prev = []
		LOD_next = []

		for k in range(len(data['ItemModel'])):
			if data['ItemModel'][k]['LOD'] not in LOD_prev:
				LOD_prev.append(data['ItemModel'][k]['LOD'])

		for k in range(len(LOD_prev)):

			if LOD_prev[k] not in check_LOD_results_array:
			
				LOD_cursor = db.cursor()
				sql = "INSERT INTO difficulty(difficulty,user_id) VALUES (%s,%s)"
				try:
					LOD_cursor.execute(sql,([LOD_prev[k]],[1]))
					db.commit()
				except:
					db.rollback()

				LOD_next.append(LOD_prev[k]+"/"+str(LOD_cursor.lastrowid))

			else:

				LOD_id = get_LOD_id(LOD_prev[k])
				LOD_next.append(LOD_prev[k]+"/"+str(LOD_id))


		for i in range(len(data['SectionModel'])):

			#Inserting the units in DB checking if its present or not
			if data['SectionModel'][i]['SectionTitle'].encode('utf-8') not in check_unit_results_array:

				unit_cursor = db.cursor()
				sql = "INSERT INTO units(name,subject_id,user_id) VALUES (%s,%s,%s)"
				try:
					unit_cursor.execute(sql,([data['SectionModel'][i]['SectionTitle'].encode('utf-8')],[subject_id],[1])) #Here 39 is subject_id
					db.commit()
				except:
					db.rollback()

				unit_cursor_lastrow = unit_cursor.lastrowid

			else:

				unit_id = get_unit_id(data['SectionModel'][i]['SectionTitle'].encode('utf-8'))
				unit_cursor_lastrow = unit_id

			chapter_topics = []
			chapters = []
			topics = []	

			#Getting the combination of chapter topic
			for k in range(len(data['QuestionPaperSectionItemModel'])):

				if data['SectionModel'][i]['QuestionPaperSectionID'] == data['QuestionPaperSectionItemModel'][k]['SectionID'] and data['ItemModel'][k]['Area'].encode('utf-8')+"/"+data['ItemModel'][k]['Topic'].encode('utf-8') not in chapter_topics:
					chapter_topics.append(data['ItemModel'][k]['Area'].encode('utf-8')+"/"+data['ItemModel'][k]['Topic'].encode('utf-8'))

			#Inserting the chapters in DB
			chapters_new = []
			topics_new = []

			for f in range(len(chapter_topics)):

				if chapter_topics[f].split("/")[0] not in chapters:

					chapters.append(chapter_topics[f].split("/")[0])

					if chapter_topics[f].split("/")[0] not in check_chapter_results_array:

						chapter_cursor = db.cursor()
						sql = "INSERT INTO chapters(name,unit_id,user_id) VALUES (%s,%s,%s)"		
						try:
							chapter_cursor.execute(sql,([chapter_topics[f].split("/")[0]],[unit_cursor_lastrow],[1]))
							db.commit()
						except:
							db.rollback()

						chapters_new.append(chapter_topics[f].split("/")[0]+"/"+str(chapter_cursor.lastrowid))

					else:

						chapter_id = get_chapter_id(chapter_topics[f].split("/")[0])
						chapters_new.append(chapter_topics[f].split("/")[0]+"/"+str(chapter_id))	

			for P in range(len(chapters_new)):
				for f in range(len(chapter_topics)):
					if chapter_topics[f].split("/")[0] == chapters_new[P].split("/")[0] and chapter_topics[f].split("/")[1] not in topics:

						topics.append(chapter_topics[f].split("/")[1])

						if chapter_topics[f].split("/")[1] not in check_topic_results_array:

							topic_cursor = db.cursor()
							sql = "INSERT INTO topics(name,chapter_id,user_id) VALUES (%s,%s,%s)"
							try:
								topic_cursor.execute(sql,([chapter_topics[f].split("/")[1]],[chapters_new[P].split("/")[1]],[1]))
								db.commit()
							except:
								db.rollback()

							topics_new.append(chapter_topics[f].split("/")[1]+"/"+str(topic_cursor.lastrowid))		

						else:

							topic_id = get_topic_id(chapter_topics[f].split("/")[1])
							topics_new.append(chapter_topics[f].split("/")[1]+"/"+str(topic_id))	
			
			#Inserting the questions and tags unitwise along with options in DB
			for k in range(len(data['QuestionPaperSectionItemModel'])):

				if data['SectionModel'][i]['QuestionPaperSectionID'] == data['QuestionPaperSectionItemModel'][k]['SectionID']:

					question_cursor = db.cursor();
					sql = "INSERT INTO questions(class_id,question,type) VALUES (%s,%s,%s)"
				
					s = u'\u092a\u094d\u0930'
					DD = u'\u092e\u0947\u0902'	
					
					soup = BeautifulSoup(data['ItemModel'][k]['Item'].split("[S]")[0],'html.parser')
					
					soup = str(soup)
					soup = soup.replace('<img' , '^')
					soup = BeautifulSoup(soup)
					soup = (soup.get_text().encode('utf-8'))
					soup = str(soup)
					soup = soup.replace('^' , '<img')

					pushpam = soup

					if len(data['ItemModel'][k]['Item'].split("[S]")) == 2:

						soup1 = BeautifulSoup(data['ItemModel'][k]['Item'].split("[S]")[1],'html.parser')

						soup1 = str(soup1)
						soup1 = soup1.replace('<img' , '^')
						soup1 = BeautifulSoup(soup1)
						soup1 = (soup1.get_text().encode('utf-8'))
						soup1 = str(soup1)
						soup1 = soup1.replace('^' , '<img')

						
						pushpam1 = soup1
					else:
						pushpam1 = ""
						# pushpam1 = ""

					# if unit_cursor_lastrow == 11: #Here 11 is last_unit_id
					# 	question_data = pushpam
					# else:
					question_data = str(pushpam) + "<br/><br/>" + str(pushpam1)

					if 'Directions : In' in pushpam or 'Directions: In' in pushpam or 'Direction : In' in pushpam or 'Direction: In' in pushpam:
						abcd = re.search(':.*?,',pushpam)
						if abcd:
							abcd = string.replace(pushpam,abcd.group(),':')
						else:
							abcd = str(pushpam)

						efgh = ''
						if s.encode('utf-8') in pushpam1:
							my_regex = s.encode('utf-8')+'.*?'+DD.encode('utf-8')
							efgh = re.sub(my_regex,'', pushpam1) 

						question_data = str(abcd) + "<br/><br/>" + str(efgh)

					elif 'Directions : Q' in pushpam or 'Directions: Q' in pushpam or 'Direction : Q' in pushpam or 'Direction: Q' in pushpam:
						abcd = re.search(':.*?,',pushpam)

						if abcd:
							abcd = string.replace(pushpam,abcd.group(),':')
						else:
							abcd = str(pushpam)

						efgh = ''
						if s.encode('utf-8') in pushpam1:
							my_regex = s.encode('utf-8')+'.*?'+DD.encode('utf-8')
							efgh = re.sub(my_regex,'', pushpam1) 

						question_data = str(abcd) + "<br/><br/>" + str(efgh)

					elif 'Direction' in pushpam or 'Directions' in pushpam:
						abcd = re.search('[(].*?[)]',pushpam)

						if abcd:
							abcd = string.replace(pushpam,abcd.group(),'')
						else:
							abcd = str(pushpam)

						efgh = re.search('[(].*?[)]',pushpam1)

						if efgh:
							efgh = string.replace(pushpam1,efgh.group(),'')
						else:
							efgh = str(pushpam1)

						question_data = str(abcd) + "<br/><br/>" + str(efgh)

					else:
						question_data = data['ItemModel'][k]['Item'].encode('utf-8').replace("[S]","<br/><br/>")

						# if s.encode('utf-8') in data['ItemModel'][k]['Item'].split("[S]")[1].encode('utf-8'):
						# 	my_regex = ''+s.encode('utf-8')+'.*?'+DD.encode('utf-8')+''
						# 	question_data = re.sub(my_regex,':', data['ItemModel'][k]['Item'].split("[S]")[1].encode('utf-8')) 
				
					try:
						question_cursor.execute(sql,([class_id],[question_data],[1])) #Here 18 is class_id
						db.commit()
					except:
						db.rollback()	

					tag_insert(question_cursor.lastrowid,subject_id	,1) #Here 39 is subject_id
					tag_insert(question_cursor.lastrowid,unit_cursor_lastrow,2)
					for j in range(len(data['ItemOptionModel'])):

						options_cursor = db.cursor()

						if data['ItemOptionModel'][j]['ItemID'] == data['ItemModel'][k]['ItemID']:
							if data['ItemOptionModel'][j]['IsCorrect'] == True:

								# if unit_cursor_lastrow == 11: #Here 11 is last_unit_id
								# 	answer_data = data['ItemOptionModel'][j]['Option'].split("[S]")[0].encode('utf-8')
								# else:
								answer_data = data['ItemOptionModel'][j]['Option'].encode('utf-8').replace("[S]","/")

								sql = "INSERT INTO options(`option`,answer,question_id) VALUES (%s,%s,%s)"
								options_cursor.execute(sql,([answer_data],[1],[question_cursor.lastrowid]))
								# print question_cursor.lastrowid
							else:

								# if unit_cursor_lastrow == 11: #Here 11 is last_unit_id
								# 	answer_data = data['ItemOptionModel'][j]['Option'].split("[S]")[0].encode('utf-8')
								# else:
								answer_data = data['ItemOptionModel'][j]['Option'].encode('utf-8').replace("[S]","/")

								sql = "INSERT INTO options(`option`,answer,question_id) VALUES (%s,%s,%s)"
								options_cursor.execute(sql,([answer_data],[0],[question_cursor.lastrowid]))

							try:
								db.commit()
							except:
								db.rollback()

				#Inserting the tags chapterwise in DB	

					for p in range(len(LOD_next)):		
						if data['ItemModel'][k]['LOD'] == LOD_next[p].split("/")[0]:
							tag_insert(question_cursor.lastrowid,LOD_next[p].split("/")[1],6)
				
					for p in range(len(chapters_new)):		
						if data['ItemModel'][k]['Area'] == chapters_new[p].split("/")[0]:				
							tag_insert(question_cursor.lastrowid,chapters_new[p].split("/")[1],3)

					for p in range(len(topics_new)):		
						if data['ItemModel'][k]['Topic'] == topics_new[p].split("/")[0]:
							tag_insert(question_cursor.lastrowid,topics_new[p].split("/")[1],4)

							tag_insert(question_cursor.lastrowid,tagging_id,5) #Here 110 is tag_id

	return redirect('/')

	

@app.route('/delete', methods=['POST'])
def del_tab():
	data=request.form['table']
	text=request.form['text']
	db=MySQLdb.connect("localhost","root","12345","elearning_test")
	cursor=db.cursor()
	sql="DELETE FROM %s"%data
	try:
		cursor.execute(sql)
		db.commit()
	except:
		db.rollback()
	db.close()
	return redirect('/')
	



if __name__ == '__main__':
    app.run() 