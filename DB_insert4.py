from flask import Flask,send_file,redirect
from flask import request
from flask import render_template

import MySQLdb
import os
import json
from os import path
from bs4 import BeautifulSoup
import re
import string

app = Flask(__name__)

@app.route('/')
def my_form():
    return render_template("index2.html")

@app.route('/show-units',methods=['POST'])
def get_units():
    files = [f for f in os.listdir(request.form['paper_path'])]
    Main = []
    for i in range(len(files)):
        X = {}
        with open(request.form['paper_path']+files[i]) as data_file:
            data = json.loads(data_file.read())
            out1 = []
            for k in range(len(data['SectionModel'])):
                out1.append(data['SectionModel'][k]['SectionTitle'])
        X['files'] = files[i]
        X['sections'] = out1
        Main.append(X)

    return render_template("index3.html",a=Main,paper_path=request.form['paper_path'])

@app.route('/edit-files',methods=['POST'])
def edit_files():
    files = [f for f in os.listdir(request.form['paper_path'])]
    section_title_array=request.form['section_names'].split(',')
    for i in range(len(files)):
        with open(request.form['paper_path']+files[i],'r+') as data_file:
        	data=json.load(data_file)
        	for j in range(len(section_title_array)):

	        	data['SectionModel'][j]['SectionTitle'] = section_title_array[j]
	        	data_file.seek(0)
	    	data_file.write(json.dumps(data))
	    	data_file.truncate()
        #     data = json.loads(data_file.read())
        #     data['SectionModel'][0]['SectionTitle'] = "Pagal"

        # with open(request.form['paper_path']+files[i], 'w') as f:
        #     f.write(json.dumps(data))
    return render_template("index3.html")

@app.route('/get-classes',methods=['POST'])
def class_func():
    db = MySQLdb.connect("localhost","root","12345","elearning_test")
    class_array = []
    class_cursor = db.cursor()
    sql = """SELECT id,name FROM classes"""
    class_cursor.execute(sql)
    class_cursor_results = class_cursor.fetchall()
    sample_array = ["id","name"]

    for x in class_cursor_results:
        class_array.append(dict(zip(sample_array,x)))

    return json.dumps(class_array)

@app.route('/get-subjects',methods=['POST'])
def subject_func():
    db = MySQLdb.connect("localhost","root","12345","elearning_test")
    subject_array = []
    subject_cursor = db.cursor()
    sql = """SELECT id,name FROM subjects WHERE class_id = '%s'""" %(request.form['class_id'])
    subject_cursor.execute(sql)
    subject_cursor_results = subject_cursor.fetchall()
    sample_array = ["id","name"]

    for x in subject_cursor_results:
        subject_array.append(dict(zip(sample_array,x)))

    return json.dumps(subject_array)

@app.route('/result1',methods=['POST'])
def mapping1():
	if request.method=='POST':
		text=request.form['name']
		db = MySQLdb.connect("localhost","root","12345",text)
		map1_cursor=db.cursor()
		map1_sql="SELECT id,name from `test_paper`"
		result=[]
		column=['id','name']
		try:
			map1_cursor.execute(map1_sql)
			for row in map1_cursor.fetchall():
				result.append(dict(zip(column,row)))
			db.commit()
		except:
			db.rollback()
		return json.dumps(result)

@app.route('/result',methods=['POST'])
def mapping():
	if request.method == 'POST':

		get_questions_dir=request.form['dir']
		text=request.form['name']
		# tagId=request.form['tag_id']


		files = [f for f in os.listdir(get_questions_dir)]
		# print files
		out1=[]
		for i in range(len(files)):
			out1.append((files[i],))
		# print out1
		db = MySQLdb.connect("localhost","root","12345",text)
		map_cursor=db.cursor()
		map_sql="SELECT	tag from tags"
		try:
			map_cursor.execute(map_sql)
			a=map_cursor.fetchall()
			print len(a)
			out=[]
			for i in range(len(a)):

				# out.append((a[i],files[i]))
				out.append(a[i])
			# print out


			column=['tag']
			column1=['paper']
			result=[]
			result1=[]


			# for row in map_cursor.fetchall():
			# 	result.append(dict(zip(column,row)))
			for row in out:
				result.append(dict(zip(column,row)))
			# print result
			for row1 in out1:
				result1.append(dict(zip(column1,row1)))
			# print result1

			map_name=json.dumps(result)
			map_name1=json.dumps(result1)
			# print map_name
			# print ''
			# print map_name1
			res=[]
			res.extend((map_name,map_name1))
			res1=json.dumps(res)
			db.commit()
		except:
			db.rollback()
		return res1

@app.route('/add', methods=['POST'])
def my_form_post():
	text = request.form['text']
	# print text
	dirr=request.form['dir']
	# print dir
	subject_id=request.form['sub_id']
	# print subject_id
	class_id=request.form['class_id']
	# print class_id
	# paper_id=request.form['test_paper_id']
	# print paper_id

	# section_titles=request.form['section_titles']
	cut_off=request.form['cut_off']
	max_num=request.form['max_num']
	neg_num=request.form['neg_num']


	db = MySQLdb.connect("localhost","root","12345",text)
	cursor=db.cursor()
	sql1="SELECT max(id) FROM `chapters`"
	sql2="SELECT max(id) FROM `topics`"
	try:
		cursor.execute(sql1)
		chapterId=cursor.fetchone();
		cursor.execute(sql2)
		topicId=cursor.fetchone();
		db.commit()
	except:
		db.rollback()


	def tag_insert(question_id,tagged_id,tag_type):

		tags_cursor = db.cursor()

		sql = "INSERT INTO tagsofquestion(question_id,tagged_id,tag_type) VALUES (%s,%s,%s)"
		try:
			tags_cursor.execute(sql,([question_id],[tagged_id],[tag_type]))
			db.commit()
		except:
			db.rollback()

	def get_LOD_id(name):

		main_id = 0
		check_LOD_cursor = db.cursor()
		sql = """SELECT id,difficulty FROM difficulty WHERE difficulty = '%s'""" % (name)
		check_LOD_cursor.execute(sql)
		check_LOD_results = check_LOD_cursor.fetchall()

		for x in check_LOD_results:
			main_id = x[0]

		return main_id

	def get_unit_id(name):

		main_id = 0
		check_unit_cursor = db.cursor()
		# sql = """SELECT id,name FROM units WHERE id > '%s' AND name = '%s'""" % (unitId,name)
		sql="SELECT id,name,subject_id FROM units WHERE name= '%s' AND subject_id = '%s'"%(name,subject_id)
		check_unit_cursor.execute(sql)
		check_unit_results = check_unit_cursor.fetchall()

		for x in check_unit_results:
			main_id = x[0]
			

		if main_id==0:
			check_unit_results_array_cursor=db.cursor()
			check_unit_results_array_sql="INSERT INTO `units` (`name`,`subject_id`, `user_id`) VALUES(%s,%s,%s)"
			try:
				check_unit_cursor.execute(check_unit_results_array_sql,([name],[subject_id],[1]))
				db.commit()
			except:
				db.rollback()
			return check_unit_results_array_cursor.lastrowid
		else:
			return main_id



	def get_chapter_id(name):

		main_id = 0
		check_chapter_cursor = db.cursor()
		sql = """SELECT id,name FROM chapters WHERE id > '%s' AND name = '%s'""" % (chapterId,name)
		check_chapter_cursor.execute(sql)
		check_chapter_results = check_chapter_cursor.fetchall()

		for x in check_chapter_results:
			main_id = x[0]

		return main_id


	def get_topic_id(name):

		main_id = 0
		check_topic_cursor = db.cursor()
		sql = """SELECT id,name FROM topics WHERE id > '%s' AND name = '%s'""" % (topicId,name)
		check_topic_cursor.execute(sql)
		check_topic_results = check_topic_cursor.fetchall()

		for x in check_topic_results:
			main_id = x[0]

		return main_id

	def question_insert(class_id,question_data):
		question_cursor = db.cursor();
		sql = "INSERT INTO questions(class_id,question,type) VALUES (%s,%s,%s)"
		try:
			question_cursor.execute(sql,([class_id],[question_data],[1])) #Here 18 is class_id
			db.commit()
		except:
			db.rollback()
		return question_cursor.lastrowid

	def soup_insert(Item):
		soup = BeautifulSoup(Item,'html.parser')
		soup = str(soup)
		soup = soup.replace('<img' , '^')
		soup = BeautifulSoup(soup)
		soup = (soup.get_text().encode('utf-8'))
		soup = str(soup)
		soup = soup.replace('^' , '<img')

		return soup

	def english_question(sarthak):
		abcd = re.search(':.*?,',sarthak)
		if abcd:
			abcd = string.replace(sarthak,abcd.group(),':')
		else:
			abcd = str(sarthak)
		return abcd

	def hindi_question(sarthak):
		efgh = ''
		if s.encode('utf-8') in sarthak:
			my_regex = s.encode('utf-8')+'.*?'+DD.encode('utf-8')
			efgh = re.sub(my_regex,'', sarthak)
		return efgh

	def english_question_new(sarthak):
		abcd = re.search('[(].*?[)]',sarthak)
		if abcd:
			abcd = string.replace(sarthak,abcd.group(),'')
		else:
			abcd = str(sarthak)
		return abcd

	def hindi_question_new(sarthak):
		efgh = re.search('[(].*?[)]',sarthak)
		if efgh:
			efgh = string.replace(sarthak,efgh.group(),'')
		else:
			efgh = str(sarthak)
		return efgh

	def options_insert(answer_data,answer,question_lastrowid):
		options_cursor = db.cursor()
		# answer_data = data['ItemOptionModel'][j]['Option'].encode('utf-8').replace("[S]","/")
		sql = "INSERT INTO options(`option`,answer,question_id) VALUES (%s,%s,%s)"
		options_cursor.execute(sql,([answer_data],[answer],[question_lastrowid]))
		try:
			db.commit()
		except:
			db.rollback()

	def passage_insert(class_id,question_data,test_paper_id,sample_section,sample_label):
		passage_cursor=db.cursor()
		passage_sql="INSERT into questions(`class_id`, `question`,`type`) values(%s,%s,%s)"
		try:
			passage_cursor.execute(passage_sql,([class_id],[question_data],[11]))
			db.commit()
		except:
			db.rollback()
		test_paper_question_insert(passage_cursor.lastrowid,test_paper_id,sample_section,sample_label)
		return passage_cursor.lastrowid

	def question_insert2(class_id,question_data,passage_lastrowid):
		question_insert2_cursor=db.cursor()
		question_insert2_sql="INSERT INTO questions(`class_id`, `question`,`type`,`parent_id`) values(%s,%s,%s,%s)"
		try:
			question_insert2_cursor.execute(question_insert2_sql,([class_id],[question_data],[1],[passage_lastrowid]))
			db.commit()
		except:
			db.rollback()
		return question_insert2_cursor.lastrowid

	def test_paper_question_insert(aa,test_paper_id,sample_section,sample_label):
		test_paper_question_cursor=db.cursor();
		test_paper_question_sql1="SELECT id,type,parent_id from questions where id='%s'"%(aa)
		test_paper_question_sql="INSERT INTO `test_paper_questions`(`test_paper_id`, `section_id`, `lable_id`, `question_id`, `max_marks`, `neg_marks`, `created`) VALUES (%s,%s,%s,%s,%s,%s,%s)"
		try:
			test_paper_question_cursor.execute(test_paper_question_sql1)
			a= test_paper_question_cursor.fetchall()
			# print a
			b= a[0][1]
			c= a[0][2]
			d=a[0][0]
			if (b==11 and c==0) or (b==1 and c==0):
				test_paper_question_cursor.execute(test_paper_question_sql,([test_paper_id],[sample_section],[sample_label],[d],[max_num],[neg_num],['0']))
				
			db.commit()
		except MySQLdb.Error,e:
			print e
			db.rollback()



	question_papers=[]
	tag_name=[]
	test_paper_id_array=[]
	test_paper_id_array=request.form.getlist('test_paper')


	# question_papers=['SSC MOCK PAPER -194.json','SSC MOCK PAPER -195.json','SSC MOCK PAPER -196.json']
	question_papers = request.form.getlist('tags')
	print question_papers
	# tag_name=['MO194','MO195','MO196']
	tag_name = request.form.getlist('srk')
	print tag_name

	get_questions_dir = dirr
	files = [f for f in os.listdir(get_questions_dir)]


	for main in range(len(files)):
		#mapping question papers with there tag names
		print files[main]
		for o in range(len(files)):
			if question_papers[o]==files[main]:
				tagging_name=tag_name[o]
				print tagging_name

		tagging_cursor=db.cursor()
		tagging_sql="SELECT id from tags where tag='%s'"%(tagging_name)
		try:
			tagging_cursor.execute(tagging_sql)
			tagging_id=tagging_cursor.fetchall()
			db.commit()
			print tagging_id
		except:
			db.rollback()

		#Check from the DB whether difficulty is present or not

		check_LOD_cursor = db.cursor()
		sql = "SELECT id,difficulty FROM difficulty"
		check_LOD_cursor.execute(sql)
		check_LOD_results = check_LOD_cursor.fetchall()
		check_LOD_results_array = []

		for x in check_LOD_results:
			if x[1] not in check_LOD_results_array:
				check_LOD_results_array.append(x[1])

		#Check from the DB whether difficulty is present or not

		#Check from the DB whether units is present or not

		check_unit_cursor = db.cursor()
		# sql = "SELECT id,name FROM units WHERE id > '%s'"%(unitId)
		sql = "SELECT id,name FROM units"
		check_unit_cursor.execute(sql)
		check_unit_results = check_unit_cursor.fetchall()
		check_unit_results_array = []

		for x in check_unit_results:
			if x[1] not in check_unit_results_array:
				check_unit_results_array.append(x[1])

		#Check from the DB whether units is present or not


		#Check from the DB whether chapters is present or not

		check_chapter_cursor = db.cursor()
		sql = "SELECT id,name FROM chapters WHERE id > '%s'"%(chapterId)
		check_chapter_cursor.execute(sql)
		check_chapter_results = check_chapter_cursor.fetchall()
		check_chapter_results_array = []

		for x in check_chapter_results:
			if x[1] not in check_chapter_results_array:
				check_chapter_results_array.append(x[1])


		#Check from the DB whether topics is present or not

		check_topic_cursor = db.cursor()
		sql = "SELECT id,name FROM topics WHERE id > '%s'"%(topicId)
		check_topic_cursor.execute(sql)
		check_topic_results = check_topic_cursor.fetchall()
		check_topic_results_array = []

		for x in check_topic_results:
			if x[1] not in check_topic_results_array:
				check_topic_results_array.append(x[1])

		#Check from the DB whether topics is present or not


		with open(get_questions_dir+files[main]) as data_file:
			data = json.loads(data_file.read())

		LOD_prev = []
		LOD_next = []

		for k in range(len(data['ItemModel'])):
			if data['ItemModel'][k]['LOD'] not in LOD_prev:
				LOD_prev.append(data['ItemModel'][k]['LOD'])

		for k in range(len(LOD_prev)):

			if LOD_prev[k] not in check_LOD_results_array:

				LOD_cursor = db.cursor()
				sql = "INSERT INTO difficulty(difficulty,user_id) VALUES (%s,%s)"
				try:
					LOD_cursor.execute(sql,([LOD_prev[k]],[1]))
					db.commit()
				except:
					db.rollback()

				LOD_next.append(LOD_prev[k]+"/"+str(LOD_cursor.lastrowid))

			else:

				LOD_id = get_LOD_id(LOD_prev[k])
				LOD_next.append(LOD_prev[k]+"/"+str(LOD_id))

		# section_array = section_titles.split(',')

		for i in range(len(data['SectionModel'])):
			section_total_ques = data['SectionModel'][i]['TotalQuestions']

			# section_total_ques.append(data['SectionModel'][i]['TotalQuestions'])

			#Inserting the units in DB checking if its present or not
			if data['SectionModel'][i]['SectionTitle'].encode('utf-8') not in check_unit_results_array:

				unit_cursor = db.cursor()
				sql = "INSERT INTO units(name,subject_id,user_id) VALUES (%s,%s,%s)"
				try:
					unit_cursor.execute(sql,([data['SectionModel'][i]['SectionTitle'].encode('utf-8')],[subject_id],[1])) #Here 39 is subject_id
					db.commit()
				except:
					db.rollback()

				unit_cursor_lastrow = unit_cursor.lastrowid

			else:

				unit_id = get_unit_id(data['SectionModel'][i]['SectionTitle'].encode('utf-8'))
				unit_cursor_lastrow = unit_id

			chapter_topics = []
			chapters = []
			topics = []

			#Getting the combination of chapter topic
			for k in range(len(data['QuestionPaperSectionItemModel'])):

				if data['SectionModel'][i]['QuestionPaperSectionID'] == data['QuestionPaperSectionItemModel'][k]['SectionID'] and data['ItemModel'][k]['Area'].encode('utf-8')+"/"+data['ItemModel'][k]['Topic'].encode('utf-8') not in chapter_topics:
					chapter_topics.append(data['ItemModel'][k]['Area'].encode('utf-8')+"/"+data['ItemModel'][k]['Topic'].encode('utf-8'))

			#Inserting the chapters in DB
			chapters_new = []
			topics_new = []

			for f in range(len(chapter_topics)):

				if chapter_topics[f].split("/")[0] not in chapters:

					chapters.append(chapter_topics[f].split("/")[0])

					if chapter_topics[f].split("/")[0] not in check_chapter_results_array:

						chapter_cursor = db.cursor()
						sql = "INSERT INTO chapters(name,unit_id,user_id) VALUES (%s,%s,%s)"
						try:
							chapter_cursor.execute(sql,([chapter_topics[f].split("/")[0]],[unit_cursor_lastrow],[1]))
							db.commit()
						except:
							db.rollback()

						chapters_new.append(chapter_topics[f].split("/")[0]+"/"+str(chapter_cursor.lastrowid))

					else:

						chapter_id = get_chapter_id(chapter_topics[f].split("/")[0])
						chapters_new.append(chapter_topics[f].split("/")[0]+"/"+str(chapter_id))

			for P in range(len(chapters_new)):
				for f in range(len(chapter_topics)):
					if chapter_topics[f].split("/")[0] == chapters_new[P].split("/")[0] and chapter_topics[f].split("/")[1] not in topics:

						topics.append(chapter_topics[f].split("/")[1])

						if chapter_topics[f].split("/")[1] not in check_topic_results_array:

							topic_cursor = db.cursor()
							sql = "INSERT INTO topics(name,chapter_id,user_id) VALUES (%s,%s,%s)"
							try:
								topic_cursor.execute(sql,([chapter_topics[f].split("/")[1]],[chapters_new[P].split("/")[1]],[1]))
								db.commit()
							except:
								db.rollback()

							topics_new.append(chapter_topics[f].split("/")[1]+"/"+str(topic_cursor.lastrowid))

						else:

							topic_id = get_topic_id(chapter_topics[f].split("/")[1])
							topics_new.append(chapter_topics[f].split("/")[1]+"/"+str(topic_id))


			# test_paper_id=int(main)+int(paper_id)
			test_paper_id=test_paper_id_array[main]
			print test_paper_id


			section_cursor = db.cursor();
			sql = "INSERT INTO section(name,total_question,cut_off_marks,test_paper,deleted) VALUES (%s,%s,%s,%s,%s)"
			# section_sql="SELECT id from `section` where name='%s'"%(section_array[z])
			try:
				# section_cursor.execute(sql,([section_array[i]],[section_total_ques],[cut_off],[test_paper_id],[0]))
				section_cursor.execute(sql,([data['SectionModel'][i]['SectionTitle']],[section_total_ques],[cut_off],[test_paper_id],[0]))
				# sample_section_id=cursor.fetchone()
				# print section_id
				db.commit()
				sample_section = section_cursor.lastrowid
			except:
				db.rollback()

			label_cursor = db.cursor();
			sql = "INSERT INTO test_paper_labels(`section_id`, `class_id`, `subject_id`, `unit_id`, `chapter_id`, `topic_id`, `question_type`, `number_of_question`, `difficulty_level`, `skill_id`, `max_marks`, `neg_marks`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
			try:
				label_cursor.execute(sql,([sample_section],[class_id],[subject_id],[0],[0],[0],[1],[section_total_ques],[0],[0],[max_num],[neg_num]))
				db.commit()
				sample_label = label_cursor.lastrowid
			except:
				db.rollback()

			passage_array=[]
			h=0
			for k in range(len(data['QuestionPaperSectionItemModel'])):

				if data['SectionModel'][i]['QuestionPaperSectionID'] == data['QuestionPaperSectionItemModel'][k]['SectionID']:
					if data['ItemModel'][k]['ItemPassageID']!=0:
						for g in range(len(passage_array)):
							if data['ItemModel'][k]['ItemPassageID']==passage_array[g]:
								h=1
								break
							else:
								h=0
						if h==0:
							passage_array.append(data['ItemModel'][k]['ItemPassageID'])
							s = u'\u092a\u094d\u0930'						
							DD = u'\u092e\u0947\u0902'
							pushpam = soup_insert(data['ItemModel'][k]['Passage'].split("[S]")[0])
							if len(data['ItemModel'][k]['Passage'].split("[S]")) == 2:
								pushpam1 = soup_insert(data['ItemModel'][k]['Passage'].split("[S]")[1])
							else:
								pushpam1 = ""
							question_data = str(pushpam) + "<br/><br/>" + str(pushpam1)
							if 'Directions : In' in pushpam or 'Directions: In' in pushpam or 'Direction : In' in pushpam or 'Direction: In' in pushpam:
								question_data = str(english_question(pushpam)) + "<br/><br/>" + str(hindi_question(pushpam1))
							elif 'Directions : Q' in pushpam or 'Directions: Q' in pushpam or 'Direction : Q' in pushpam or 'Direction: Q' in pushpam:
								question_data = str(english_question(pushpam)) + "<br/><br/>" + str(hindi_question(pushpam1))
							elif 'Direction' in pushpam or 'Directions' in pushpam:
								question_data = str(english_question_new(pushpam)) + "<br/><br/>" + str(hindi_question_new(pushpam1))
							else:
								question_data = data['ItemModel'][k]['Passage'].encode('utf-8').replace("[S]","<br/><br/>")
							passage_lastrowid=passage_insert(class_id,question_data,test_paper_id,sample_section,sample_label)

						s = u'\u092a\u094d\u0930'						
						DD = u'\u092e\u0947\u0902'
						pushpam = soup_insert(data['ItemModel'][k]['Item'].split("[S]")[0])
						if len(data['ItemModel'][k]['Item'].split("[S]")) == 2:
							pushpam1 = soup_insert(data['ItemModel'][k]['Item'].split("[S]")[1])
						else:
							pushpam1 = ""
						question_data = str(pushpam) + "<br/><br/>" + str(pushpam1)
						if 'Directions : In' in pushpam or 'Directions: In' in pushpam or 'Direction : In' in pushpam or 'Direction: In' in pushpam:
							question_data = str(english_question(pushpam)) + "<br/><br/>" + str(hindi_question(pushpam1))
						elif 'Directions : Q' in pushpam or 'Directions: Q' in pushpam or 'Direction : Q' in pushpam or 'Direction: Q' in pushpam:
							question_data = str(english_question(pushpam)) + "<br/><br/>" + str(hindi_question(pushpam1))
						elif 'Direction' in pushpam or 'Directions' in pushpam:
							question_data = str(english_question_new(pushpam)) + "<br/><br/>" + str(hindi_question_new(pushpam1))
						else:
							question_data = data['ItemModel'][k]['Item'].encode('utf-8').replace("[S]","<br/><br/>")
						question_lastrowid=question_insert2(class_id,question_data,passage_lastrowid)

					else:

						# question_cursor = db.cursor();
						# sql = "INSERT INTO questions(class_id,question,type) VALUES (%s,%s,%s)"

						s = u'\u092a\u094d\u0930'
						DD = u'\u092e\u0947\u0902'

						# soup = BeautifulSoup(data['ItemModel'][k]['Item'].split("[S]")[0],'html.parser')

						# soup = str(soup)
						# soup = soup.replace('<img' , '^')
						# soup = BeautifulSoup(soup)
						# soup = (soup.get_text().encode('utf-8'))
						# soup = str(soup)
						# soup = soup.replace('^' , '<img')

						pushpam = soup_insert(data['ItemModel'][k]['Item'].split("[S]")[0])

						if len(data['ItemModel'][k]['Item'].split("[S]")) == 2:

							# soup1 = BeautifulSoup(data['ItemModel'][k]['Item'].split("[S]")[1],'html.parser')

							# soup1 = str(soup1)
							# soup1 = soup1.replace('<img' , '^')
							# soup1 = BeautifulSoup(soup1)
							# soup1 = (soup1.get_text().encode('utf-8'))
							# soup1 = str(soup1)
							# soup1 = soup1.replace('^' , '<img')


							pushpam1 = soup_insert(data['ItemModel'][k]['Item'].split("[S]")[1])
						else:
							pushpam1 = ""
							# pushpam1 = ""

						# if unit_cursor_lastrow == 11: #Here 11 is last_unit_id
						# 	question_data = pushpam
						# else:
						question_data = str(pushpam) + "<br/><br/>" + str(pushpam1)

						if 'Directions : In' in pushpam or 'Directions: In' in pushpam or 'Direction : In' in pushpam or 'Direction: In' in pushpam:
							# abcd = re.search(':.*?,',pushpam)
							# if abcd:
							# 	abcd = string.replace(pushpam,abcd.group(),':')
							# else:
							# 	abcd = str(pushpam)

							# efgh = ''
							# if s.encode('utf-8') in pushpam1:
							# 	my_regex = s.encode('utf-8')+'.*?'+DD.encode('utf-8')
							# 	efgh = re.sub(my_regex,'', pushpam1)

							question_data = str(english_question(pushpam)) + "<br/><br/>" + str(hindi_question(pushpam1))

						elif 'Directions : Q' in pushpam or 'Directions: Q' in pushpam or 'Direction : Q' in pushpam or 'Direction: Q' in pushpam:
							# abcd = re.search(':.*?,',pushpam)

							# if abcd:
							# 	abcd = string.replace(pushpam,abcd.group(),':')
							# else:
							# 	abcd = str(pushpam)

							# efgh = ''
							# if s.encode('utf-8') in pushpam1:
							# 	my_regex = s.encode('utf-8')+'.*?'+DD.encode('utf-8')
							# 	efgh = re.sub(my_regex,'', pushpam1)

							question_data = str(english_question(pushpam)) + "<br/><br/>" + str(hindi_question(pushpam1))

						elif 'Direction' in pushpam or 'Directions' in pushpam:
							# abcd = re.search('[(].*?[)]',pushpam)

							# if abcd:
							# 	abcd = string.replace(pushpam,abcd.group(),'')
							# else:
							# 	abcd = str(pushpam)

							# efgh = re.search('[(].*?[)]',pushpam1)

							# if efgh:
							# 	efgh = string.replace(pushpam1,efgh.group(),'')
							# else:
							# 	efgh = str(pushpam1)

							question_data = str(english_question_new(pushpam)) + "<br/><br/>" + str(hindi_question_new(pushpam1))

						else:
							question_data = data['ItemModel'][k]['Item'].encode('utf-8').replace("[S]","<br/><br/>")

							# if s.encode('utf-8') in data['ItemModel'][k]['Item'].split("[S]")[1].encode('utf-8'):
							# 	my_regex = ''+s.encode('utf-8')+'.*?'+DD.encode('utf-8')+''
							# 	question_data = re.sub(my_regex,':', data['ItemModel'][k]['Item'].split("[S]")[1].encode('utf-8'))

						# try:
						# 	question_cursor.execute(sql,([class_id],[question_data],[1])) #Here 18 is class_id
						# 	db.commit()
						# except:
						# 	db.rollback()

						question_lastrowid=question_insert(class_id,question_data)


					tag_insert(question_lastrowid,15,1) #Here 39 is subject_id
					tag_insert(question_lastrowid,unit_cursor_lastrow,2)
					for j in range(len(data['ItemOptionModel'])):

						options_cursor = db.cursor()

						if data['ItemOptionModel'][j]['ItemID'] == data['ItemModel'][k]['ItemID']:
							if data['ItemOptionModel'][j]['IsCorrect'] == True:


								# answer_data = data['ItemOptionModel'][j]['Option'].encode('utf-8').replace("[S]","/")

								# sql = "INSERT INTO options(`option`,answer,question_id) VALUES (%s,%s,%s)"
								# options_cursor.execute(sql,([answer_data],[1],[question_lastrowid]))
								# # print question_lastrowid
								options_insert(data['ItemOptionModel'][j]['Option'].encode('utf-8').replace("[S]","/"),1,question_lastrowid)
							else:


								# answer_data = data['ItemOptionModel'][j]['Option'].encode('utf-8').replace("[S]","/")

								# sql = "INSERT INTO options(`option`,answer,question_id) VALUES (%s,%s,%s)"
								# options_cursor.execute(sql,([answer_data],[0],[question_lastrowid]))

								options_insert(data['ItemOptionModel'][j]['Option'].encode('utf-8').replace("[S]","/"),0,question_lastrowid)

							# try:
							# 	db.commit()
							# except:
							# 	db.rollback()


				#Inserting the tags chapterwise in DB

					for p in range(len(LOD_next)):
						if data['ItemModel'][k]['LOD'] == LOD_next[p].split("/")[0]:
							tag_insert(question_lastrowid,LOD_next[p].split("/")[1],6)

					for p in range(len(chapters_new)):
						if data['ItemModel'][k]['Area'] == chapters_new[p].split("/")[0]:
							tag_insert(question_lastrowid,chapters_new[p].split("/")[1],3)

					for p in range(len(topics_new)):
						if data['ItemModel'][k]['Topic'] == topics_new[p].split("/")[0]:
							tag_insert(question_lastrowid,topics_new[p].split("/")[1],4)

							tag_insert(question_lastrowid,tagging_id,5) #Here 110 is tag_id


					# test_paper_question_cursor=db.cursor();
					# test_paper_question_sql1="SELECT id,type,parent_id from questions where id='%s'"%(passage_lastrowid)
					# test_paper_question_sql="INSERT INTO `test_paper_questions`(`test_paper_id`, `section_id`, `lable_id`, `question_id`, `max_marks`, `neg_marks`, `created`) VALUES (%s,%s,%s,%s,%s,%s,%s)"
					# try:
					# 	test_paper_question_cursor.execute(test_paper_question_sql1)
					# 	a= test_paper_question_cursor.fetchall()
					# 	print a
					# 	b= a[0][1]
					# 	c= a[0][2]
					# 	d=a[0][0]
					# 	if (b==11 and c==0) or (b==1 and c==0):
					# 		test_paper_question_cursor.execute(test_paper_question_sql,([test_paper_id],[sample_section],[sample_label],[d],[max_num],[neg_num],['0']))
							
					# 	db.commit()
					# except MySQLdb.Error,e:
					# 	print e
					# 	db.rollback()
					test_paper_question_insert(question_lastrowid,test_paper_id,sample_section,sample_label)

	return redirect('/')

@app.route('/delete', methods=['POST'])
def del_tab():
	data=request.form['table']
	text=request.form['text']
	db=MySQLdb.connect("localhost","root","12345","elearning_test")
	cursor=db.cursor()
	sql="DELETE FROM %s"%data
	try:
		cursor.execute(sql)
		db.commit()
	except:
		db.rollback()
	db.close()
	return redirect('/')




if __name__ == '__main__':
    app.run(debug=True)
